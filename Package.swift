// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "HafeleSmartphoneKeySDK_Light",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "dialock-ios-sdk-public-light",
            targets: ["dialock-ios-sdk-public-light"]),
    ],
    dependencies: [
    ],
    targets: [
        .binaryTarget(
            name: "HafeleSmartphoneKeySDK_Light",
            path: "Sources/HafeleSmartphoneKeySDK_Light.xcframework"
        ),
        .target(
            name: "dialock-ios-sdk-public-light",
            dependencies: [
                "HafeleSmartphoneKeySDK_Light"
            ]
        ),
    ]
)
